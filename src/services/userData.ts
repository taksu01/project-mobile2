import { User } from './../interface/user.interface';

export class UserDataService{
    user: User = {
        id: null,
        name: null,
        score: 0,
        avatar: 0
    };

    initializeUser(id,name,score,avatar){
        this.user.id = id;
        this.user.name = name;
        this.user.score = score;
        this.user.avatar = avatar;
    }

    changeName(name){
        this.user.name = name;
    }

    changeScore(score){
        this.user.score = score;
    }

    changeAvatar(avatar){
        this.user.avatar = avatar;
    }
}