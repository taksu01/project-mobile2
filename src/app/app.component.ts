
import { UserDataService } from './../services/userData';
import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import {GameRoomPage} from'../pages/game-room/game-room';
import {RoomListPage} from '../pages/room-list/room-list';
import { WaitingRoomPage } from './../pages/waiting-room/waiting-room';
import firebase from "firebase";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, userDataSvc: UserDataService) {
    platform.ready().then(() => {

      firebase.initializeApp({
        apiKey: "AIzaSyDY7U9Mp218AiGKXANVZRVUfntkrwR7zGo",
        authDomain: "project-mobile2-262e1.firebaseapp.com",
        databaseURL: "https://project-mobile2-262e1.firebaseio.com",
        projectId: "project-mobile2-262e1",
        storageBucket: "project-mobile2-262e1.appspot.com",
        messagingSenderId: "781409513635"
      })

      firebase.auth().onAuthStateChanged(user => {
        if(user) {
          //do something here if the user is logged in
          userDataSvc.initializeUser(user.uid,user.displayName,0,0);
        }
        else 
        {
          //do something here if the user is not logged in
          userDataSvc.initializeUser(null,null,0,0);
        }
      });
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

