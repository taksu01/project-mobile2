
import { ScorePage } from './../pages/score/score';
import { AnswersPage } from './../pages/answers/answers';




import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { GameRoomPage } from '../pages/game-room/game-room';
import { RoomListPage } from '../pages/room-list/room-list';
import { WaitingRoomPage } from './../pages/waiting-room/waiting-room';
import { CreateRoomPage } from '../pages/create-room/create-room';
import { CreditsPage } from './../pages/credits/credits';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';

//services
import { AuthService } from './../services/auth';
import { UserDataService } from './../services/userData';
import { ServerService } from './../services/server';
//native
import { NativeAudio } from '@ionic-native/native-audio';

export const firebaseConfig = {
  apiKey: "AIzaSyDY7U9Mp218AiGKXANVZRVUfntkrwR7zGo",
  authDomain: "project-mobile2-262e1.firebaseapp.com",
  databaseURL: "https://project-mobile2-262e1.firebaseio.com",
  projectId: "project-mobile2-262e1",
  storageBucket: "project-mobile2-262e1.appspot.com",
  messagingSenderId: "781409513635"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    GameRoomPage,
    RoomListPage,
    CreateRoomPage,
    WaitingRoomPage,
    CreditsPage,
    AnswersPage,
    ScorePage
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    GameRoomPage,
    RoomListPage,
    CreateRoomPage,
    WaitingRoomPage,
    CreditsPage,
    AnswersPage,
    ScorePage
  ],
  providers: [
    NativeAudio,
    StatusBar,
    SplashScreen,
    AuthService,
    UserDataService,
    ServerService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
