import { ServerService } from './../../services/server';
import { CreditsPage } from './../credits/credits';
import { UserDataService } from './../../services/userData';
import { AuthService } from './../../services/auth';
import { GameRoomPage } from './../game-room/game-room';
import { RoomListPage } from './../room-list/room-list';
import { Component } from '@angular/core';
import { NavController, ToastController, ModalController } from 'ionic-angular';
import * as io from 'socket.io-client';
import firebase from "firebase";
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from 'angularfire2/database';
import { NativeAudio } from '@ionic-native/native-audio';

import {
  trigger,
  state,
  style,
  animate,
  transition,
  keyframes
} from '@angular/animations';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  animations: [
    trigger('bounce', [
      state('bouncing', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('* => bouncing', [
        animate('300ms ease-in', keyframes([
          style({transform: 'translate3d(0,0,0)', offset: 0}),
          style({transform: 'translate3d(0,-12px,0)', offset: 0.6}),
          style({transform: 'translate3d(0,0,0)', offset: 1})
        ]))
      ]),
      transition('bouncing => *', [
        animate('300ms ease-in', keyframes([
          style({transform: 'translate3d(0,0,0)', offset: 0}),
          style({transform: 'translate3d(0,-12px,0)', offset: 0.6}),
          style({transform: 'translate3d(0,0,0)', offset: 1})
        ]))
      ])
    ])
  ]
})

export class HomePage {

 
  socket:any
  chat_input:string;
  chats = [];
  gameStart:boolean;

  signedIn:boolean;
  signUp:boolean;

  signup_name="";
  signup_email="";
  signup_password="";
  signup_confirm="";
  signup_avatar=1;

  username = "";
  password = "";

  bounceState: String = 'noBounce';
  musicPlay = true;
  

  constructor(public navCtrl: NavController,public authSvc: AuthService,public userDataSvc: UserDataService, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public nativeAudio: NativeAudio, public modalCtrl: ModalController, public serverSvc: ServerService, public alertCtrl: AlertController) {
    
    this.nativeAudio.preloadComplex('menubgm', './assets/audio/blueskies.mp3', 1, 1, 0).then(
      ()=>{
        this.nativeAudio.loop('menubgm');
        //this.nativeAudio.play('menubgm');
      },
      error => {
        console.log(error.message);
      }
    )
    //title animation here
    this.toggleBounce();
  }

  

  toggleBounce(){
    setInterval(() => {
      this.bounceState = (this.bounceState == 'noBounce') ? 'bouncing' : 'noBounce';
    }, 1500);
  }

  toggleMusic(){
    this.musicPlay = !this.musicPlay;
    if(!this.musicPlay) {
      this.nativeAudio.stop('menubgm');
    } else {
      this.nativeAudio.loop('menubgm');
    }
  }

  singUp(){
    this.signUp = true;
  }

  signUp_button_click(){
    if(this.signup_password !== this.signup_confirm){
      let toast = this.toastCtrl.create({
        message: "Password confirmation does not match!",
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
    }

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
  
    loading.present();

    this.authSvc.signup(this.signup_email,this.signup_password).then(
      (user)=>{
        loading.dismiss();
        //put user data to firebase
        console.log(user.uid);    

        
      },
      error=>{
        loading.dismiss();
        let toast = this.toastCtrl.create({
          message: error.message,
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
      }
    );
  }

  already_have_account_click(){
    this.signUp = false;
    
  }

  login(){
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
  
    loading.present();

    this.authSvc.signin(this.username,this.password).then(
      ()=>{
        loading.dismiss();
      },
      error=>{
        loading.dismiss();
        console.log(error.message);
        let toast = this.toastCtrl.create({
          message: "invalid username or password",
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
      }
    );;
  }

  logout(){
    this.authSvc.logout();
  }
      
  play(){
    /*if(!this.gameStart){
        this.socket.emit('gameState',"start");
        this.gameStart = true;
    }*/

    this.navCtrl.push(RoomListPage);
  }

  check_login_status(){
    if(this.userDataSvc.user.id != null){
      return true;
    } else {
      return false;
    }
  }

  showCredits(){
    let createRoomModal = this.modalCtrl.create(CreditsPage);
    createRoomModal.present();
  }

  setServerIp(){
    let alert = this.alertCtrl.create({
      title: 'Current IP ('+this.serverSvc.serverIp+')',
      inputs: [
        {
          name: 'newIp',
          placeholder: 'New IP'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Set',
          handler: data => {
            this.serverSvc.setIp(data.newIp);
          }
        }
      ]
    });
    alert.present();
  }
}
