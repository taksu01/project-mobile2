import { ServerService } from './../../services/server';
import { UserDataService } from './../../services/userData';
import { Observable } from 'rxjs/Observable';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from 'angularfire2/database';
import {GameRoomPage} from './../game-room/game-room';
import * as io from 'socket.io-client';
/**
 * Generated class for the WaitingRoomPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-waiting-room',
  templateUrl: 'waiting-room.html',
})
export class WaitingRoomPage {
  socket:any;
  chat_input:string;
  chats = [];

 
  
  players: Observable<any[]>;
  roomMaster: AngularFireObject<any>;
  roomMasterId = "";

  roomData = {
    id: null,
    name: null,
    bg: 1,
    players: 1
  };
  constructor(public navCtrl: NavController, public navParams: NavParams, public afDatabase: AngularFireDatabase, public userDataSvc: UserDataService, public serverSvc: ServerService) {
    this.socket = io('http://'+this.serverSvc.serverIp+':3001');

    this.socket.on('message'+this.navParams.data.id, (msg)=>{
      console.log("message", msg);
      this.chats.push(msg);
    });
    this.roomData = this.navParams.data;
    this.players = this.afDatabase.list('rooms/'+this.roomData.id+"/playingPlayer").valueChanges();
    this.checkRoomMaster()

    this.socket.on('play'+this.navParams.data.id, (msg)=>{
      this.navCtrl.push(GameRoomPage, this.roomData);
    })

    setInterval(()=>{
      this.checkRoomMaster();
    },6000);
  }

  async checkRoomMaster(){
    console.log("refresh room master");
    //this.roomMasterId = this.afDatabase.object('rooms/'+this.roomData.id+"/playingPlayer")
    var roomMasterOrder = 9 //room master is the first one in the group
    //console.log(this.players[0]);
    //console.log(this.players);
    await this.players.forEach(player=>{
      player.forEach(p =>{
        //console.log(p);
        if(p.order < roomMasterOrder){
          roomMasterOrder = p.order;
          this.roomMasterId = p.playerId;
        }
        //console.log(this.roomMasterId);
      })
      //console.log(this.roomMasterId);
    })
    return this.roomMasterId;
    //console.log(this.roomMasterId);
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WaitingRoomPage');
    //console.log(this.navParams.data);
  }

  back_click(){
    //remove himself from room DB
    this.afDatabase.object('rooms/'+this.roomData.id+"/playingPlayer/"+this.userDataSvc.user.id).remove().then(
      ()=>{
        //check if the room is empty
        console.log("check if room is empty");
        this.players.forEach(
          players=>{
            if(players.length == 0){
              //room is empty, therefore delete room
              this.afDatabase.object('rooms/'+this.roomData.id).remove()
            }
          }
        )
      }
    );
    this.navCtrl.pop();
  }

  getAvatar(avatarNum){
    return "./assets/imgs/avatar/"+avatarNum+".png";
  }

  getBackground(){
    return "background"+this.roomData.bg;
  }

  countPlayer(roomPlayer){
    //console.log(roomPlayer)
    console.log(this.checkRoomMaster());
  }
  send(msg){
    if(msg!=' '){
      this.socket.emit('message',this.userDataSvc.user.name +" : "+msg, this.roomData.id);
      this.chat_input = '';
      console.log('send message');
    }
  }
  play(){
    this.socket.emit('play', this.roomData.id);
    
  }
}