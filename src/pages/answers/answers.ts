import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the AnswersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-answers',
  templateUrl: 'answers.html',
})
export class AnswersPage {

  answers = [];
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    this.answers = this.navParams.data;
    console.log(this.answers);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AnswersPage');
  }

  ionViewWillEnter(){
    this.answers = this.navParams.data;
    console.log(this.answers);
  }

  back_click(){
    this.viewCtrl.dismiss();
  }

}
