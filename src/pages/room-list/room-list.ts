import { WaitingRoomPage } from './../waiting-room/waiting-room';
import { UserDataService } from './../../services/userData';
import { CreateRoomPage } from './../create-room/create-room';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController } from 'ionic-angular';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from 'angularfire2/database';

/**
 * Generated class for the RoomListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

import {
  trigger,
  state,
  style,
  animate,
  transition,
  keyframes
} from '@angular/animations';
import { Observable } from 'rxjs/Observable';
import { List } from 'ionic-angular/components/list/list';


@IonicPage()
@Component({
  selector: 'page-room-list',
  templateUrl: 'room-list.html',
  animations: [
    trigger('flyIn', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('* => in', [
        animate('300ms ease-in', keyframes([
          style({transform: 'translate3d(0,200px,0)', offset: 0}),
          style({transform: 'translate3d(0,0,0)', offset: 1})
        ]))
      ])
    ])
  ]
})
export class RoomListPage {
  firstRun: boolean = false;
  senseiState = "off";

  avatarNum = 6; //IMPORTANT: for now it only has 3 avatars
  
  displayName = "";
  displayAvatar = 1;


  selectedRoom;
  rooms: AngularFireList<any>; //this one is used to push, update, or delete room data
  items: Observable<any[]>; //IMPORTANT: this variable holds the room data
  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public afDatabase: AngularFireDatabase, public userDataSvc: UserDataService, public toastCtrl: ToastController) {
    this.rooms = afDatabase.list('rooms');
    this.items = afDatabase.list('rooms').valueChanges();
    
  }

  setAccount(){
    this.afDatabase.object('user/data/'+this.userDataSvc.user.id).update({
        name: this.displayName,
        avatar: this.displayAvatar,
        score: 0
      })

      var tes: AngularFireObject<any>;
      tes = this.afDatabase.object('user/data/'+this.userDataSvc.user.id);
      console.log("AF OBJECT:"+ tes.query);
      console.log(tes);

      this.firstRun = false;
  }

  getAvatar(avatarNum){
    return "./assets/imgs/avatar/"+avatarNum+".png";
  }

  prev_avatar_click(){
    this.displayAvatar--;
    if(this.displayAvatar<1){
      this.displayAvatar = this.avatarNum;
    }
  }

  next_avatar_click(){
    this.displayAvatar++;
    if(this.displayAvatar>this.avatarNum){
      this.displayAvatar = 1;
    }
  }


  ionViewDidLoad() {

    //console.log('ionViewDidLoad RoomListPage');
    this.senseiState = "in";
    if(this.userDataSvc.user.name == null || this.userDataSvc.user.avatar == 0){
      //this.firstRun = true;
      //load avatar from database
      
      var charData: Observable<any>;
      this.afDatabase.list('user/data/'+this.userDataSvc.user.id).valueChanges().subscribe(items=>{
        console.log(items);
        this.userDataSvc.changeAvatar(items[0]);
        this.userDataSvc.changeName(items[1]);
        this.userDataSvc.changeScore(items[2]);

        if(items.length == 0) this.firstRun = true;
      })

    }
  }

  back_click(){
    this.navCtrl.pop();
  }

  async getRoomDetail(roomId:string){
    this.items.map(items=>{
      return items.filter(item => item.id === roomId)[0]
    }).subscribe(
      singleItem => {
        this.selectedRoom = singleItem;
      }
    )
    //console.log(this.selectedRoom);
  }

  getRoomDetailByArray(index){
    this.items.subscribe(items=>{
      this.selectedRoom = items[index];
    })
  }

  join_room(room_id){
    this.getRoomDetail(room_id).then(
      ()=>{
        console.log("selected room:");
        //console.log(this.selectedRoom);
      })
    
    setTimeout(()=>{
      console.log(this.selectedRoom); //change code to move_room
      if(this.selectedRoom.id == null){
        //error join room
        let toast = this.toastCtrl.create({
          message: "Can't join game room, please check your internet connection",
          duration: 3000,
          position: 'bottom'
        });

        toast.present();
        return;
      } else {
        //check room availability
        var currentPlayer = this.countPlayer(this.selectedRoom.playingPlayer);
        if(currentPlayer >= this.selectedRoom.players){
          //room is full
          let toast = this.toastCtrl.create({
            message: "Can't join game room, room is full",
            duration: 3000,
            position: 'bottom'
          });
  
          toast.present();
          return;
        }
        //log player to the room DB
        var roomData: AngularFireObject<any>;
        roomData = this.afDatabase.object('rooms/'+this.selectedRoom.id+'/playingPlayer/'+this.userDataSvc.user.id);
        
        var newPlayerRef = roomData.update({
          name: this.userDataSvc.user.name,
          playerId: this.userDataSvc.user.id,
          avatar: this.userDataSvc.user.avatar,
          order: currentPlayer
        });

      }

      
      this.navCtrl.push(WaitingRoomPage,this.selectedRoom);
    },1000);
  }

  countPlayer(roomPlayer){
    //console.log(roomPlayer)
    var count = 0
    for(var i in roomPlayer){
      count++;
     }
    //console.log(count);
    return count;
  }

  async create_room_click(){
    
    
    

    // this.items.forEach(item=>{
    //   //console.log(item);
    // })

    let createRoomModal = this.modalCtrl.create(CreateRoomPage);
    createRoomModal.present();
    createRoomModal.onDidDismiss(data => {
      if(data!=null){
        console.log("room create");
        console.log(data);
        
        const newRoomRef = this.rooms.push({});
        
        newRoomRef.set({
          id: newRoomRef.key,
          name: data.name,
          players: data.players,
          bg: data.bg
        }).then(()=>{
          this.join_room(newRoomRef.key);
        });

        //console.log(this.rooms);
        //console.log(this.items);
      } else {
        console.log("Create room canceled");
      }
    })
    //this.navCtrl.push(CreateRoomPage);
  }
}
