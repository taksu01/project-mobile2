import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the CreateRoomPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-create-room',
  templateUrl: 'create-room.html',
})
export class CreateRoomPage {

  roomName = "";
  players = 5;
  bg = 1;
  bgNumber = 3; //for now theres only 2 number of bg, IMPORTANT: when release, update the value of 2
  bgName = "./assets/imgs/create-room/"+"bg1.png";

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateRoomPage');
  }

  decrease_player_click(){
    this.players--;
    if(this.players < 1){
      this.players = 6;
    }
  }

  increase_player_click(){
    this.players++;
    if(this.players > 6){ //max number of players is 6
      this.players = 1;
    }
  }

  next_bg_click(){
    this.bg++;
    if(this.bg > this.bgNumber){ 
      this.bg = 1;
    }

    this.bgName = "./assets/imgs/create-room/"+"bg"+this.bg+".png";
  }


  prev_bg_click(){
    this.bg--;
    if(this.bg < 1){ 
      this.bg = this.bgNumber;
    }

    this.bgName = "./assets/imgs/create-room/"+"bg"+this.bg+".png";
  }

  create_room_click(){
    var options = {
      name: this.roomName,
      bg: this.bg,
      players: this.players
    }

    this.viewCtrl.dismiss(options);
  }

  back_button_click(){
    this.viewCtrl.dismiss();
  }
}
