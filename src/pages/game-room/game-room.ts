import { ScorePage } from './../score/score';
import { AnswersPage } from './../answers/answers';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , ModalController} from 'ionic-angular';
import * as io from 'socket.io-client';
import {RoomListPage} from './../room-list/room-list';
import { UserDataService } from './../../services/userData';
import { NativeAudio } from '@ionic-native/native-audio';
import { ServerService } from '../../services/server';
/**
 * Generated class for the GameRoomPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-game-room',
  templateUrl: 'game-room.html',
})
export class GameRoomPage {
  win = false;
  winner = "";
  socket:any;
  question:string;
  questions = [];
  chats =[];
  answers = [];
  chat_input:string;
  gameStart:boolean;
  playerId = 0;
  gameFinish:boolean = false;
  temp=0;
  scores = [];
  roomData = {
    id: null,
    name: null,
    bg: 1,
    players: 1
  };

  showBubble = {
    display:'none'
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, public userDataSvc: UserDataService, public modalCtrl: ModalController, public nativeAudio: NativeAudio, public serverSvc: ServerService) {
    this.gameStart = false;
    this.roomData = this.navParams.data;
    

    this.nativeAudio.preloadComplex('correct', './assets/audio/correct.mp3', 1, 1, 0);

    console.log(this.roomData);

    this.socket = io('http://'+this.serverSvc.serverIp+':3000'); //server addres

    
    this.socket.on('playerNumber', (number)=>{
      if(this.playerId==0)
      this.playerId = number;

      console.log('I am player ' + this.playerId.toString());
    })
    this.socket.on('quiz',(quiz)=>{
     
      
      this.questions.push(quiz);
      this.question = quiz;

      //this.chats.push(quiz);
      
    });

    this.socket.on('finish',(finish)=>{
      console.log("The game has finish, your score is " + finish);

      this.gameFinish = true;
      
      for(var i = 0;i<11;i++){
        if(finish[i]>finish[i+1]){
          this.temp = i;
        }
      }
      this.question = "Total score "+finish[this.playerId].toString();
    });

    this.socket.on('finish2',(finish2)=>{

      this.win = true;
      this.winner = "Winner "+ finish2[this.temp].toString();
     
    });

    this.socket.on('score'+this.navParams.data.id, (score)=>{
      this.scores = score;
    })
    this.socket.on('message'+this.navParams.data.id, (msg) => {
      console.log("message", msg);
      this.chats.push(msg);
      
    });
    this.socket.on('answers'+this.navParams.data.id, (msg) => {
      console.log("answer", msg);
      this.answers.push(msg);
      this.nativeAudio.play('correct');
    });
    this.socket.on("nextQ", (msg=>{
      this.answers= [];
    }))


    this.socket.on('correct'+this.navParams.data.id, (msg)=>{
      if(msg.toString() == this.playerId) {
        console.log(this.userDataSvc.user.name+" you answer correctly!");

        //user answer correctly
        this.showBubble = {
          display:'block'
        };;

        setTimeout(()=>{
          this.showBubble = {
            display:'none'
          };;
        },3000);
      }

    })
    this.play();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GameRoomPage');
  }

  send(msg) {
    this.chat_input ="";
    this.socket.emit("jawaban", msg, this.playerId, this.navParams.data.id,this.userDataSvc.user.name );
    
  }
  goBack(){
    this.navCtrl.pop();
  }

  getBackground(){
    return "background"+this.roomData.bg;
  }

  getBubbleStatus(){
    return this.showBubble;
  }

  play(){
    if(!this.gameStart){
       this.socket.emit('gameState',"start");
       this.gameStart = true;
    }
  }

  showAnswers(){
    var modal = this.modalCtrl.create(AnswersPage,this.answers);
    modal.present();
  }

  showScores(){
    var modal = this.modalCtrl.create(ScorePage,this.scores);
    modal.present();
  }

}
